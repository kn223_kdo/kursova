﻿using AutoMapper;
using Kursa4.Models;
using Kursa4.Models.ViewModel;

namespace Kursa4.Mapper
{
	public class MapperProfile : Profile
	{
		public MapperProfile() 
		{
			CreateMap<CreateUserDTO, Userio>().ReverseMap();
			CreateMap<CreateAuthorDTO, Author>().ReverseMap();
			CreateMap<CreateBookDTO, Book>().ReverseMap();
			CreateMap<UptadeAuthorDTO, Author>().ReverseMap();
			CreateMap<UpdateUserDTO, Userio>().ReverseMap();
			CreateMap<UpdateBookDTO, Book>().ReverseMap();
		}
	}
}
