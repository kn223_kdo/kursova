﻿using Microsoft.AspNetCore.Identity;

public static class WebUiExtensions
{
    public static IdentityBuilder AddAppIdentity<TUser>(this IServiceCollection services) where TUser : class
    {
        return services.AddCoreIdentity<TUser>(_ => { });
    }

    public static IdentityBuilder AddCoreIdentity<TUser>(this IServiceCollection services, Action<IdentityOptions> configureOptions) where TUser : class
    {
        return services.AddIdentityCore<TUser>(o =>
        {
            o.Stores.MaxLengthForKeys = 128;
            configureOptions?.Invoke(o);
        })
            .AddDefaultUI();
    }
}