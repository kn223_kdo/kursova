﻿using System.Data.SqlTypes;
using Azure;

namespace Kursa4.Models;

public class SearchResultUni<T, Query>
{
    public List<T>? List { get; set; }
    public int Page { get; set; }
    public Query? SearchQuery { get; set; }
}