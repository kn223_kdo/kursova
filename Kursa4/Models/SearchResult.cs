﻿namespace Kursa4.Models;

public class SearchResult
{
    public List<Userio>? Users { get; set; }
    public List<Author>? Authors { get; set; }
    public List<Book>? Books { get; set; }
}