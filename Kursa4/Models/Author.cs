﻿using System.ComponentModel.DataAnnotations;

namespace Kursa4.Models
{
    public class Author
    {
        public string AuthorName { get; set; }
        [Key]
        public int AuthorId { get; set; }
        public List<AuthorsBooks>? AuthorsBooks { get; set;}
    }
}
