﻿namespace Kursa4.Models;

public class SearchAuthorQuery
{
    public string AuthorName { get; set; }
    public bool IsAlph { get; set; }
}