﻿using Microsoft.EntityFrameworkCore.Design;

namespace Kursa4.Models;

public class SearchBookQuery
{
    public string Title { get; set; }
    public string Disributor { get; set; }
    public string SortProperty { get; set; }
    public bool IsAlph { get; set; }
}