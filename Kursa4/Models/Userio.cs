﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace Kursa4.Models
{
    public class Userio
    {
        public List<Book>? Books { get; set; }
        [Key]
        public int? UserId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public List<UserBooks>? UserBooks { get; set; }
    }
}
