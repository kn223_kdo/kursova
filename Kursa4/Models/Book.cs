﻿using System.ComponentModel.DataAnnotations;

namespace Kursa4.Models
{
    public class Book
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Disributor { get; set; }
        public string Description { get; set; }
        public List<AuthorsBooks>? AuthorsBooks { get; set;}
        public List<UserBooks>? UserBooks { get; set; }
        public List<Userio>? Users { get; set; }
    }
}
