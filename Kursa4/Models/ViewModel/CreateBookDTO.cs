﻿namespace Kursa4.Models.ViewModel;

public class CreateBookDTO
{
    public string Title { get; set; }
    public string Disributor { get; set; }
    public string Description { get; set; }
}