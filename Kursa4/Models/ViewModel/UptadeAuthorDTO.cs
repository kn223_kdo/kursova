﻿namespace Kursa4.Models.ViewModel;

public class UptadeAuthorDTO
{
    public string? AuthorName { get; set; }
    public int AuthorId { get; set; }
}