﻿namespace Kursa4.Models.ViewModel;

public class UserBooksDTO
{
    public int BookId { get; set; }
    public int UserId { get; set; }
    public List<Book>? Books { get; set; }
}