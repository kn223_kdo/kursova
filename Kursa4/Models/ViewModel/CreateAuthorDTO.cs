﻿using System.ComponentModel.DataAnnotations;

namespace Kursa4.Models.ViewModel
{
    public class CreateAuthorDTO
    {
        public string? AuthorName { get; set; }
    }
}
