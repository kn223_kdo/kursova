﻿namespace Kursa4.Models.ViewModel;

public class UpdateBookDTO
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string Disributor { get; set; }
    public string Description { get; set; }
}