﻿namespace Kursa4.Models.ViewModel;

public class AuthorBooksDTO
{
    public int AuthorId { get; set; }
    public int BookId { get; set; }
    public List<Book>? Books { get; set; }
}