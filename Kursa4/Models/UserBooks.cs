﻿namespace Kursa4.Models;

public class UserBooks
{
    public int? BookId { get; set; }
    public int? UserID { get; set; }
    public Book Book { get; set; }
    public Userio User { get; set; }
}