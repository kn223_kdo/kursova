﻿namespace Kursa4.Models;

public class SearchUserQuery
{
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public bool isAlph { get; set; }
}