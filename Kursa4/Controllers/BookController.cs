﻿using AutoMapper;
using Lib.Data.Repositories;
using Kursa4.Models;
using Kursa4.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace Kursa4.Controllers
{
    [Route("book")]
    public class BookController : Controller
    {
        private readonly UowRepository _uowRepository;
        private readonly IMapper _mapper;

        public BookController(UowRepository uowRepository, IMapper mapper)
        {
            _uowRepository = uowRepository;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("{bookid}")]
        public async Task<ActionResult> IndexAsync(int bookid)
        {
            var book = await _uowRepository.GenericRepository<Book>().FindAsync(x => x.Id == bookid, "AuthorsBooks.Author");
            return View("Book", book);
        }
        [HttpPost]
        [Route("create")]
        public async Task<ActionResult> CreateBook(CreateBookDTO createBookDto)
        {
            var book = await _uowRepository.GenericRepository<Book>().CreateAsync(_mapper.Map<Book>(createBookDto));
            await _uowRepository.SaveAsync();
            return Redirect($"{book.Id}");
        }
        [HttpGet]
        [Route("form")]
        public async Task<ActionResult> GetBookForm()
        {
            return View("CreateBook");
        }

        [HttpGet]
        [Route("update/{bookId}")]
        public async Task<ActionResult> GetUpdateForm(int bookID)
        {
            var book = await _uowRepository.GenericRepository<Book>().FindAsync(x => x.Id == bookID);
            return View("GetUpdateBook", _mapper.Map<UpdateBookDTO>(book));
        }
        [HttpPost]
        public async Task<ActionResult> Update(UpdateBookDTO updateBookDto)
        {
            var upd = await _uowRepository.GenericRepository<Book>().UpdateAsync(_mapper.Map<Book>(updateBookDto));
            await _uowRepository.SaveAsync();
            return Redirect($"book/{upd.Id}");
        }

        [HttpGet]
        [Route("delete/{id}")]
        public async Task<ActionResult> DeleteBook(int id)
        {
            await _uowRepository.GenericRepository<Book>().DeleteAsync(new Book
            {
                Id = id
            });
            await _uowRepository.SaveAsync();
            return RedirectToAction("GetBookForm");
        }
    }
}
