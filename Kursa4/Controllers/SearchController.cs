﻿using System.Numerics;
using AutoMapper;
using Lib.Data.Repositories;
using Kursa4.Models;
using Kursa4.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace Kursa4.Controllers;
[Route("search")]
public class SearchController : Controller
{
    private readonly UowRepository _uowRepository;
    [HttpPost]
    public async Task<ActionResult> Search(Search q)
    {
        string search = q.search;
        SearchResult searchResult = new SearchResult();
        searchResult.Authors = (await _uowRepository.GenericRepository<Author>().GetAsync(x
            => x.AuthorName.Contains(search))).ToList();
        searchResult.Users =
            (await _uowRepository.GenericRepository<Userio>().GetAsync(x
                => x.FirstName.Contains(search) || x.LastName.Contains(search))).ToList();
        searchResult.Books = (await _uowRepository.GenericRepository<Book>().GetAsync(x
            => x.Title.Contains(search) || x.Disributor.Contains(search))).ToList();
        return View("SearchResult", searchResult);
    }
    public SearchController(UowRepository uowRepository)
    {
        _uowRepository = uowRepository;
    }
    [HttpGet]
    [Route("form")]
    public ActionResult FormSearch()
    {
        return View("SearchForm", new Search());
    }
    
    [HttpPost]
    [Route("books")]
    public async Task<ActionResult> SearchBooks(SearchBookQuery q)
    {
       
        if (q.SortProperty.Equals("Title"))
        {
            var los = (await _uowRepository
                .GenericRepository<Book>()
                .GetAsync(x => x.Title.Contains(q.Title) ||
                               x.Disributor.Contains(q.Disributor), orderBy: x=> 
                        x.OrderBy(x => x.Title), includeProperties:"AuthorsBooks.Author"));
            if (!los.Any())
            {
                los = await _uowRepository.GenericRepository<Book>()
                    .GetAsync(x => x.Title.Contains(x.Title) ||
                                   x.Disributor.Contains(x.Disributor), orderBy: x=> 
                        x.OrderBy(x => x.Title), includeProperties:"AuthorsBooks.Author");
            }
            if (!q.IsAlph)
            {
                los = los.Reverse();
            }
            return View("FilterAndSearch", new SearchResultUni<Book,SearchBookQuery>
            {
                List = los.ToList(), SearchQuery = q
            });
        }
        var booksResult = _uowRepository
            .GenericRepository<Book>()
            .GetAsync(x => x.Title.Contains(q.Title) ||
                           x.Disributor.Contains(q.Disributor), includeProperties:"AuthorsBooks.Author");
        return View("FilterAndSearch", new SearchResultUni<Book,SearchBookQuery>
        {
            List = (await booksResult).ToList(), SearchQuery = q
        });
    }

    [HttpGet]
    [Route("book")]
    public ActionResult SearchForm()
    {
        return View("FilterAndSearch", new SearchResultUni<Book,SearchBookQuery>());
    }

    [HttpPost]
    [Route("authors")]
    public async Task<ActionResult> SearchAuthor(SearchAuthorQuery q)
    {
        var authors = (await _uowRepository.GenericRepository<Author>().GetAsync(x 
            => x.AuthorName.Contains(q.AuthorName)));
        if (!authors.Any())
        {
            authors = await _uowRepository.GenericRepository<Author>()
                .GetAsync(x => x.AuthorName.Contains(x.AuthorName));
        }
        if (!q.IsAlph)
        {
            authors = authors.Reverse();
        }
        return View("FilterAndSearchAuthor", new SearchResultUni<Author, SearchAuthorQuery>
        {
            List = authors.ToList(), SearchQuery = q
        });
    }

    [HttpGet]
    [Route("author/find")]
    public ActionResult SearchAuthorForm()
    {
        return View("FilterAndSearchAuthor", new SearchResultUni<Author, SearchAuthorQuery>());
    }

    [HttpGet]
    [Route("user/find")]
    public ActionResult SearchUserForm()
    {
        return View("FilterAndSearchUser", new SearchResultUni<Userio, SearchUserQuery>());
    }

    [HttpPost]
    [Route("users")]
    public async Task<ActionResult> SearchUser(SearchUserQuery q)
    {
        var users = (await _uowRepository.GenericRepository<Userio>().GetAsync(x 
                => x.FirstName.Contains(q.FirstName) || x.LastName.Contains(q.LastName)));
        if (!users.Any())
        {
            users = await _uowRepository.GenericRepository<Userio>().GetAsync(x
                => x.FirstName.Contains(x.FirstName));
        }
        if (q.isAlph)
        {
            users = users.Reverse();
        }
        return View("FilterAndSearchUser", new SearchResultUni<Userio, SearchUserQuery>
        {
            List = users.ToList(), SearchQuery = q
        });
    }
}