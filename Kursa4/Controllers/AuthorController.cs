﻿using AutoMapper;
using Lib.Data.Repositories;
using Kursa4.Models;
using Kursa4.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace Kursa4.Controllers
{
    [Route("author")]
    public class AuthorController : Controller
    {
        private readonly UowRepository _uowRepository;
        private readonly IMapper _mapper;
        public AuthorController(UowRepository uowRepository, IMapper mapper)
        {
            _uowRepository = uowRepository;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("{authorid}")]
        public async Task<ActionResult> IndexAsync(int authorid)
        {
            var author = await _uowRepository.GenericRepository<Author>().FindAsync(x => x.AuthorId == authorid, "AuthorsBooks.Book");
            return View("Author", author);
        }
        [HttpPost]
        [Route("create")]
        public async Task<ActionResult> CreateAuthor(CreateAuthorDTO createAuthorDTO)
        {
            var author = await _uowRepository.GenericRepository<Author>().CreateAsync(_mapper.Map<Author>(createAuthorDTO));
            await _uowRepository.SaveAsync();
            return Redirect($"{author.AuthorId}");
        }
        [HttpGet]
        [Route("form")]
        public async Task<ActionResult> GetAuthorForm()
        {
            return View("CreateAuthor");
        }
        [HttpGet]
        [Route("update")]
        public async Task<ActionResult> GetUpdateForm(int authorId)
        {
            var author = await _uowRepository.GenericRepository<Author>().FindAsync(x => x.AuthorId == authorId);
            return View("GetUpdateForm", _mapper.Map<UptadeAuthorDTO>(author));
        }
        [HttpPost]
        [Route("update")]
        public async Task<ActionResult> Update(UptadeAuthorDTO uptadeAuthorDto)
        {
            var upd = await _uowRepository.GenericRepository<Author>().UpdateAsync(_mapper.Map<Author>(uptadeAuthorDto));
            await _uowRepository.SaveAsync();
            return Redirect($"{upd.AuthorId}");
        }
        [Route("author/addbook")]
        public async Task<ActionResult> AddBook(AuthorsBooks authorsBooks)
        {
            var addbook = await _uowRepository.GenericRepository<AuthorsBooks>().CreateAsync(authorsBooks);
            await _uowRepository.SaveAsync();
            return Redirect($"/author/{addbook.AuthorId}");
        }

        [HttpGet]
        [Route("book/add/{authorId}")]
        public async Task<ActionResult> GetAddBookForm(int authorId)
        {
            return View("AddBook", new AuthorBooksDTO() 
            {
                AuthorId = authorId, Books = (await _uowRepository.GenericRepository<Book>().GetAsync()).ToList()
            });
        }

        [HttpGet]
        [Route("delete/{id}")]
        public async Task<ActionResult> DeleteAuthor(int id)
        {
            await _uowRepository.GenericRepository<Author>().DeleteAsync(new Author
            {
                AuthorId = id
            });
            await _uowRepository.SaveAsync();
            return RedirectToAction("GetAuthorForm");
        }
        
        [HttpGet]
        [Route("{authorId}/book/{bookId}")]
        public async Task<ActionResult> DelBook(int authorId, int bookId)
        {
            await _uowRepository.GenericRepository<AuthorsBooks>().DeleteAsync(new AuthorsBooks()
            {
                AuthorId = authorId, BookId = bookId
            });
            await _uowRepository.SaveAsync();
            return RedirectToAction("GetAuthorForm");
        }
    }
}
