﻿using AutoMapper;
using Lib.Data.Repositories;
using Kursa4.Models;
using Kursa4.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace Kursa4.Controllers
{
    [Route("user")]
    public class UserController : Controller
    {
        private readonly UowRepository _uowRepository;
        private readonly IMapper _mapper;

        public UserController(UowRepository uowRepository, IMapper mapper)
        {
            _uowRepository = uowRepository;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("{userid}")]
        public async Task<ActionResult> IndexAsync(int userid)
        {
            var user = await _uowRepository.GenericRepository<Userio>().FindAsync(x => x.UserId == userid, "UserBooks.Book.AuthorsBooks.Author");
            return View("User", user);
        }
        [HttpPost]
        [Route("create")]
        public async Task<ActionResult> CreateUser(CreateUserDTO createUserDTO)
        {
            var user = await _uowRepository.GenericRepository<Userio>().CreateAsync(_mapper.Map<Userio>(createUserDTO));
            await _uowRepository.SaveAsync();
            return Redirect($"{user.UserId}");
        }
        [HttpGet]
        [Route("form")]
        public async Task<ActionResult> GetUserForm()
        {
            return View("Create");
        }

        [HttpGet]
        [Route("update/{userId}")]
        public async Task<ActionResult> GetUpdateForm(int userId)
        {
            var user = await _uowRepository.GenericRepository<Userio>().FindAsync(x => x.UserId == userId);
            return View("GetUpsateForm", _mapper.Map<UpdateUserDTO>(user));
        }

        [HttpPost]
        [Route("update")]
        public async Task<ActionResult> Update(UpdateUserDTO updateUserDto)
        {
            var upd = await _uowRepository.GenericRepository<Userio>().UpdateAsync(_mapper.Map<Userio>(updateUserDto));
            await _uowRepository.SaveAsync();
            return Redirect($"{upd.UserId}");
        }
        [HttpPost]
        [Route("user/books")]
        public async Task<ActionResult> AddBook(UserBooks userBooks)
        {
            var addbook = await _uowRepository.GenericRepository<UserBooks>().CreateAsync(userBooks);
            await _uowRepository.SaveAsync();
            return Redirect($"/user/{addbook.UserID}");
        }

        [HttpGet]
        [Route("book/add/{userId}")]
        public async Task<ActionResult> GetAddBookForm(int userId)
        {
            return View("AddBook", new UserBooksDTO
            {
                UserId = userId, Books = (await _uowRepository.GenericRepository<Book>().GetAsync()).ToList()
            });
        }

        [HttpGet]
        [Route("delete/{id}")]
        public async Task<ActionResult> DeleteUser(int id)
        {
            await _uowRepository.GenericRepository<Userio>().DeleteAsync(new Userio
            {
                UserId = id
            });
            await _uowRepository.SaveAsync();
            return RedirectToAction("GetUserForm");
        }

        [HttpGet]
        [Route("{userId}/book/{bookId}")]
        public async Task<ActionResult> ReturnBook(int userId, int bookId)
        {
            await _uowRepository.GenericRepository<UserBooks>().DeleteAsync(new UserBooks()
            {
                UserID = userId, BookId = bookId
            });
            await _uowRepository.SaveAsync();
            return RedirectToAction("Index", new { userId });
        }
    }
}
