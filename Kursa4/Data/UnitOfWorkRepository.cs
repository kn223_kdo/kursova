﻿using Kursa4.Data;
using Lib.Infrastructure.Repository;
using Microsoft.Extensions.Logging;

namespace Lib.Data.Repositories
{
    public class UowRepository : IDisposable
    {
        private readonly AppDBContext _appDbContext;
        private readonly RepositoryFactory _repositoryFactory;
        private Dictionary<string, object>? _repositories;

        public UowRepository(RepositoryFactory repositoryFactory,
            AppDBContext forumDbContext)
        {
            _repositoryFactory = repositoryFactory;
            _appDbContext = forumDbContext;
        }
        public void Dispose()
        {
            _appDbContext.Dispose();
        }

        public GenericRepository<T> GenericRepository<T>() where T : class
        {
            if (_repositories == null)
            {
                _repositories = new Dictionary<string, object>();
            }

            var type = typeof(T).Name;
            if (!_repositories.ContainsKey(type))
            {
                var repository = _repositoryFactory.Instance<T>(_appDbContext);
                _repositories.Add(type, repository);
            }

            return (GenericRepository<T>)_repositories[type];
        }

        public Task SaveAsync()
        {
            try
            {
                return _appDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}