﻿using Kursa4.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Kursa4.Data;

public class AuthorConfig : IEntityTypeConfiguration<Author>
{
        public void Configure(EntityTypeBuilder<Author> builder)
        {
                builder.HasKey(p => p.AuthorId);
        }
        
}