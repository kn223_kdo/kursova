﻿using Kursa4.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Kursa4.Data;

public class UserBooksConfigurations : IEntityTypeConfiguration<UserBooks>
{
    public void Configure(EntityTypeBuilder<UserBooks> builder)
    {
        builder.HasKey(pt => new
        {
            pt.UserID,
            pt.BookId
        });

        builder.HasOne(
                p => p.User)
            .WithMany(p => p.UserBooks)
            .HasForeignKey(p => p.UserID);

        builder.HasOne(
                p => p.Book)
            .WithMany(p => p.UserBooks)
            .HasForeignKey(p => p.BookId);
    }

}