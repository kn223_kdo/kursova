﻿using Kursa4.Models;
using Microsoft.EntityFrameworkCore;

namespace Kursa4.Data
{
    public class AppDBContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Userio> Users { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<AuthorsBooks> AuthorsBooks { get; set; }
        public DbSet<UserBooks> UserBooks { get; set; }
        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AuthorBookConfiguration());
            modelBuilder.ApplyConfiguration(new UserBooksConfigurations());
            modelBuilder.ApplyConfiguration(new AuthorConfig());
            modelBuilder.ApplyConfiguration(new BookConfig());
            base.OnModelCreating(modelBuilder);
            
        }
    }
}
