﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Kursa4.Models;


public class AuthorBookConfiguration : IEntityTypeConfiguration<AuthorsBooks>
{
    public void Configure(EntityTypeBuilder<AuthorsBooks> builder)
    {
        builder.HasKey(pt => new
        {
            pt.AuthorId,
            pt.BookId
        });

        builder.HasOne(
                p => p.Book)
            .WithMany(p => p.AuthorsBooks)
            .HasForeignKey(p => p.BookId);

        builder.HasOne(p => p.Author)
            .WithMany(p => p.AuthorsBooks)
            .HasForeignKey(p => p.AuthorId);
    }
}